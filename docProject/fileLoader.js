const allowedExtensions = ['pptx', 'doc', 'ppt'];
let fileInput;


const listenOnce = function(el, event, func){
    el.addEventListener(event, function(){
        func();
        el.removeEventListener(event, func);
    });
};

const presentFile = (function(){
    let url = '';
    return {
        getFname: function(){
            return url;
        },
        setFname: function(newUrl){
            url = newUrl;
        } 
    };
})();
// const setHeader = function(ext){
//     return {
//         'pptx':'multipart/form-data',
//         // 'pptx':'application/vnd.openxmlformats-officedocument.presentationml.presentation',
//         'ppt':'application/vnd.ms-powerpoint',
//         'doc':'application/msword'
//     }[ext];
// };

const deleteFrameNode = function(){
    const frame = document.getElementById('fileView');
    const body = document.getElementsByTagName('body')[0];
    if (frame){
        body.removeChild(frame);
        console.log('old frame removed');
    }else{
        console.log('there is no frame yet');
    }
};


const deleteFile = function(filename){
    deleteFrameNode();
    const xhr = new XMLHttpRequest();
    xhr.open('POST', location.origin + '/api/delete_file' , true);
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(JSON.stringify({fname: filename}));
    xhr.onreadystatechange = function(){
        if (this.readyState === XMLHttpRequest.DONE){
            if (this.status === 200){
                console.log('succeed');
            }
        }
    };
};


// returns promise, 
const sendFile = function (file, fext) {
    return new Promise(function (resolve, reject) {
        const data = new FormData(document.getElementById('form'));
        data.append('ext', fext);
        const xhr = new XMLHttpRequest();
        xhr.open('POST', location.origin + '/api/save_file', true);
        xhr.send(data);
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE) {
                if (this.status === 200) {
                    const fpath = JSON.parse(this.responseText).fpath;

                    presentFile.setFname(fpath);

                    window.addEventListener('beforeunload', function(e){
                        deleteFile(fpath);
                    });

                    resolve(JSON.parse(this.responseText));
                } else {
                    reject();
                }
            }
        };

    });
};

// const sendFile = function(){
//     const form = document.getElementById('form');

//     form.submit(function(e){
//         e.preventDefault();
//     });  
// };

const createFrameNode = function(url){
    const frame = document.createElement('iframe');
    const body = document.getElementsByTagName('body')[0];
    const attributes = {
        src: url,
        id: "fileView",
        style:"width:800px; height:900px;",
        frameborder: "0"
    };
    for (key in attributes){
        frame.setAttribute(key, attributes[key]);
    }
    console.log(body);
    body.prepend(frame);
    return frame;
};


// const viewFile = async function (file, fext) {
//     const directory = await sendFile(file, fext);
//     console.log(directory);
//     const frame = document.getElementById('fileView');
//     const url = location.origin + '/' + directory.fpath;
//     console.log(url);
//     frame.setAttribute('src','https://docs.google.com/viewer?url=' +  url + '.pptx&embedded=true');
// };

const hideForm = function(){
    const form = document.getElementsByTagName('form')[0];
    form.classList.add('ghost');
    // returns function , which shows the invisible button
    return function(){
        form.classList.remove('ghost');
    };
};

const fileView = async function (file, fext) {
    const showAgain = hideForm();
    deleteFile(presentFile.getFname());
    const directory = await sendFile(file, fext);
    const url = location.origin + '/' + directory.fpath;
    frame = createFrameNode('https://docs.google.com/viewer?url=' +  url + '&embedded=true');
    listenOnce(frame, 'load', showAgain);
    // maybe change to showAgain
};

const validExtension = function (file) {

    const fext = file.name.slice(file.name.indexOf('.') + 1, file.name.length);
    if (allowedExtensions.includes(fext)) {
        return fext;
    }
    return false;
    
};

const handleNewFile = function(file){
    let fext;
    if (fext=validExtension(file)){
        fileView(file, fext);
    }else{
        alert('unsupported file extension');
    }
};


fileInput = document.getElementById('file');
fileInput.addEventListener('change', function (e) {
                                           handleNewFile(fileInput.files[0]);
                                        });