const express = require('express');
const app = express();
const multer = require('multer')({dest: 'upload/'});
const fs = require('fs');

// configs 
app.use(express.static('./upload'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


//static directories
app.use(express.static(__dirname+'/../upload'));
app.use(express.static(__dirname+'/../docProject'));


// main page
app.get('/', (req, res) => {
	res.sendFile(__dirname + "/index.html");
});

app.post('/api/save_file', multer.single('name'),  (req, res)=>{
	console.log('sent');
	console.log(req.file);
	if (req.file && req.body.ext){
		fs.renameSync(__dirname + '/../upload/' + req.file.filename, __dirname + '/../upload/' + req.file.filename + '.' + req.body.ext);
		res.status(200);
		res.json({
			fpath: req.file.filename + '.' + req.body.ext
		});
	}else{
		res.status(404);
		res.json({
			error: "sorry, no required files"
		});
	}
});

app.post('/api/delete_file', function(req, res){
	console.log(req.body);
	if (req.body && req.body.fname){
		console.log(__dirname + '/../upload/' + req.body.fname);
		fs.unlink(__dirname + '/../upload/' + req.body.fname, function(err){
			if (err){
				//handle error here
				console.log(err);
			}
			console.log('file deleted');
		});
		res.status(200);
	}
});

app.listen(process.env.PORT || 3000);